<?php

namespace app\admin\model;

use think\Model;

class BaseModel extends Model
{
    //数据库连接
    protected $connection = 'sys_mysql';
    // 数据转换为驼峰命名
    protected $convertNameToCamel = true;

}