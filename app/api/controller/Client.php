<?php
declare (strict_types = 1);

namespace app\api\controller;

use hg\apidoc\annotation as Apidoc;

/**
 * 客户端
 * @package app\api\controller3
 */
class Client
{
    /**
     * @Apidoc\Title("获取客户端IP地址")
     * @author 贾二小
     * @since 2021/7/30
     */
    public function ip(){
        return get_client_ip(0, true);
    }

}