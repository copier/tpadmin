<?php


namespace app\common\service;


use service\ToolsService;
use think\facade\Db;

/**
 * 流水号生成
y - 年份的两位数表示
Y - 年份的四位数表示
m - 月份的数字表示（从 01 到 12）
M - 月份的短文本表示（用三个字母表示）
d - 一个月中的第几天（从 01 到 31）
D - 星期几的文本表示（用三个字母表示）
g - 12 小时制，不带前导零（1 到 12）
G - 24 小时制，不带前导零（0 到 23）
h - 12 小时制，带前导零（01 到 12）
H - 24 小时制，带前导零（00 到 23）
i - 分，带前导零（00 到 59）
s - 秒，带前导零（00 到 59）
 * @author 贾二小
 * @since 2021/3/28
 */
class SerialNumberService
{
    /**
     * 获取流水号
     * @param string $serial_number_alias
     * @return mixed|string
     * @author 贾二小
     * @since 2021/3/28
     */
    public static function getSerialNumberByNo($serial_number_alias = ''){
        $cur_value = '';
        Db::startTrans();
        try{
            $map = [];
            $map[] = ['alias', '=', $serial_number_alias];
            $map[] = ['status', '=', 1];
            $map[] = ['is_delete', '=', 0];
            $serial_umber_data = Db::table('s_serial_number')
                ->where($map)
                ->field('id,regulation,gen_type,no_length,cur_date,init_value,cur_value,step')
                ->find();
            if(empty($serial_umber_data))
                ToolsService::error('获取流水号异常');
            $now_date = datetime(time());
            $cur_date = $serial_umber_data['cur_date'];
            $gen_type = $serial_umber_data['gen_type'];
            $cur_value = $serial_umber_data['cur_value'];
            if($gen_type == 8){
                //按照每日生成
                if($now_date != $cur_date){
                    $cur_value = $serial_umber_data['init_value'];
                }else{
                    $cur_value = $cur_value + $serial_umber_data['step'];
                }
            }elseif ($gen_type == 9){
                //按照每月生成
                if(substr($now_date, 0, 7) != substr($cur_date, 0, 7)){
                    $cur_value = $serial_umber_data['init_value'];
                }else{
                    $cur_value = $cur_value + $serial_umber_data['step'];
                }
            }elseif (substr($now_date, 0, 4) != substr($cur_date, 0, 4)){
                //按照每年生成
                if($gen_type != $cur_date){
                    $cur_value = $serial_umber_data['init_value'];
                }else{
                    $cur_value = $cur_value + $serial_umber_data['step'];
                }
            }else{
                //递增 gen_type = 11
                $cur_value = $cur_value + $serial_umber_data['step'];
            }
            $regulation_arr = [];
            $pre = $serial_umber_data['regulation'];
            preg_match_all("#{([^}]+)}#us", $serial_umber_data['regulation'], $regulation_arr);
            foreach ($regulation_arr[1] as $key=>$vo){
                switch ($vo)
                {
                    case 'Y':
                    case 'y':
                    case 'M':
                    case 'm':
                    case 'D':
                    case 'd':
                    case 'H':
                    case 'h':
                    case 'G':
                    case 'g':
                    case 'i':
                    case 's':
                        $txt = date($vo);
                        break;
                    default:
                        $txt = '';
                }
                $pre = str_replace($regulation_arr[0][$key],$txt,$pre);
            }
            Db::table('s_serial_number')->where(['id'=>$serial_umber_data['id']])->update(['cur_date'=>$now_date, 'cur_value'=>$cur_value]);
            $cur_value = str_pad($cur_value, $serial_umber_data['no_length'], '0', STR_PAD_LEFT);
            $cur_value = $pre.$cur_value;
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            ToolsService::error($e->getMessage());
        }
        return $cur_value;
    }

}