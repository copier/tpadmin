<?php

namespace app\common\service;

use OSS\OssClient;
use OSS\Core\OssException;

class OssService
{
    /**
     * @param string $fileName 文件名称
     * @param string $filePath 由本地文件路径加文件名包括后缀组成，例如/users/local/myFile.txt
     * @return array 返回访问地址和异常信息
     * @author 贾二小
     * @since 2021/8/5
     */
    public static function up(string $fileName,string $filePath): array
    {
        //阿里云RAM账号AccessKey
        $accessKeyId = get_config("aliyunAccessKeyId");
        $accessKeySecret = get_config("aliyunAccessKeySecret");
        //Endpoint以杭州为例，其它Region请按实际情况填写。
        $endpoint = get_config("aliyunEndpoint");
        //设置存储空间名称。
        $bucket= get_config("aliyunBucket");
        $data = [];
        try{
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $data = $ossClient->uploadFile($bucket, $fileName, $filePath);
            $data['success'] = true;
        } catch(OssException | \Exception $e) {
            $data['success'] = false;
            $data['errorMsg'] = $e->getMessage();
        }
        return $data;
    }

}