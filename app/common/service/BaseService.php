<?php


namespace app\common\service;


use think\facade\Db;

class BaseService
{

    protected $field = '';
    protected $table_name = '';
    protected $info_sql = '';
    protected $page_sql = '';
    protected $id_page = true;

    public function __construct()
    {
        $this->field = '*';
    }

    public function info($id){
        if(empty($this->info_sql)){
            $this->info_sql = 'SELECT '.($this->field).' FROM `'.($this->table_name).'` WHERE `id` = '.$id;
        }

        $data = Db::query($this->info_sql);
        if(count($data) > 0){
            return $data[0];
        }else{
            return null;
        }
    }

    public function list(){
        if(empty($this->info_sql)){
            $this->info_sql = 'SELECT '.($this->field).' FROM `'.($this->table_name).'`';
        }
        return Db::query($this->info_sql);
    }

    public function page($where=[]){
        $sql_where = ' WHERE 1=1';
        //拼接搜索条件
        foreach ($where as $vo){
            if(is_string($vo)){
                $vo = [$vo,];
            }
            $vo_count = count($vo);
            if($vo_count === 0){
                continue;
            }
            $vo_value = input($vo[0]);
            if(in_array($vo_count,[1,2]) && empty($vo_value)){
                continue;
            }else{
                if($vo_count === 1){
                    $sql_where .= ' AND a.'.$vo[0].' = "'.$vo_value.'"';
                    continue;
                }else{
                    if($vo[1] == 'equal'){
                        $sql_where .= ' AND a.'.$vo[0].' = "'.$vo_value.'"';
                    }elseif ($vo[1] == 'like'){
                        $sql_where .= ' AND a.'.$vo[0].' LIKE "%'.$vo_value.'%"';
                    }elseif ($vo[1] == 'like_left'){
                        $sql_where .= ' AND a.'.$vo[0].' LIKE "%'.$vo_value.'"';
                    }elseif ($vo[1] == 'like_right'){
                        $sql_where .= ' AND a.'.$vo[0].' LIKE "'.$vo_value.'%"';
                    }elseif ($vo[1] == 'in'){
                        $vo_value = json_encode($vo_value,true);
                        $vo_value = substr($vo_value,1,strlen($vo_value));
                        $sql_where .= ' AND a.'.$vo[0].' in ('.$vo_value.')';
                    }
                }
            }
            if($vo_count == 4){
                //暂定只有区间查询
                $vo_value_start = input($vo[2]);
                $vo_value_end = input($vo[3]);
                if($vo[1] == 'between'){
                    if(!empty($vo_value_start) && !empty($vo_value_end)){
                        $sql_where .= ' AND a.'.$vo[0].' BETWEEN "'.$vo_value_start.'" AND "'.$vo_value_start.'"';
                    }
                }
            }
        }

        if(empty($this->page_sql)){
            $sql = 'SELECT '.($this->field).' FROM `'.($this->table_name).'` a';
        }else{
            $sql = 'SELECT a.* FROM ('.($this->page_sql).') a';
        }

        if($this->id_page){
            if(empty($this->page_sql)){
                $sql_count = 'SELECT count(1) as c FROM `'.($this->table_name).'` a';
            }else{
                $sql_count = 'SELECT count(1) as c FROM ('.($this->page_sql).') a';
            }
            $page = intval(input('page',1));
            $limit = intval(input('limit',10));
            $offset = ($page-1)*$limit;
            $count = 0;
            $count_data = Db::query($sql_count.$sql_where);
            if(!empty($count_data)){
                $count = $count_data[0]['c'];
            }
            $data = Db::query($sql.$sql_where." LIMIT {$offset},{$limit}");
            return [
                'data'=>$data,
                'count'=>$count,
            ];
        }else{
            $data = Db::query($sql.$sql_where);
            return [
                'data'=>$data,
                'count'=>0,
            ];
        }
    }


//    public function delete($ids){
//        $sql = 'UPDATE `'.($this->field).'` SET `is_delete` = 1  WHERE `id` in ('.($ids).')';
//        return Db::execute($sql);
//    }



}