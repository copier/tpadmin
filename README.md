# TPAdmin

## 介绍
使用最新的PHPThink6和layuiadmin搭建的后台权限管理系统


## 安装教程
1.  克隆项目&安装插件
~~~
# 使用Git克隆当前项目
git clone https://gitee.com/liwunbings/tpadmin.git
# 进入当前项目
cd tpadmin
# 安装插件
composer install
~~~

2. 导入数据库文件到本地，并新建.env文件
~~~
APP_DEBUG = true

[APP]
DEFAULT_TIMEZONE = Asia/Shanghai

[DATABASE]
TYPE = mysql
HOSTNAME = 127.0.0.1
DATABASE = tpadmin
USERNAME = root
PASSWORD = 123465++
HOSTPORT = 3306
CHARSET = utf8
DEBUG = true

[LANG]
default_lang = zh-cn
~~~
3. PHP运行项目或手动配置到public目录访问
~~~
php think run
#手动配置的在此文档中不展示，可百度(Apche24+php8+mysql8或Nginx+php8+mysql8)
~~~

## 演示地址

[后台页面演示地址](https://tp.ipsoul.com)

![](https://tp.ipsoul.com/static/img/admin.png)

~~~
演示用户名：admin
演示密码：123456
~~~

[接口文档演示地址](https://doc.ipsoul.com/)

![](https://tp.ipsoul.com/static/img/api.png)

~~~
演示授权密码：123456
~~~

## 文档

[ThinkPHP6完全开发手册](https://www.kancloud.cn/manual/thinkphp6_0/content)

### 支持作者

如果觉得框架不错，或者已经在使用了，希望你可以去 Gitee 帮我点个 ⭐ Star，这将是对我极大的鼓励与支持。