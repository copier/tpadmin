/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : tpadmin

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/09/2021 15:13:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for aliyun_ip
-- ----------------------------
DROP TABLE IF EXISTS `aliyun_ip`;
CREATE TABLE `aliyun_ip` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `infocode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `adcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `rectangle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of aliyun_ip
-- ----------------------------
BEGIN;
INSERT INTO `aliyun_ip` VALUES (1, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (2, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (3, '223.104.4.227', '1', 'OK', '10000', '江苏省', '南京市', '320000', '118.4253323,31.80452471;119.050169,32.39401346');
INSERT INTO `aliyun_ip` VALUES (4, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (5, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (6, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (7, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (8, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (9, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (10, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (11, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (12, '117.83.148.104', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (13, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (14, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (15, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (16, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (17, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (18, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (19, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (20, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (21, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (22, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (23, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (24, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (25, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (26, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (27, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (28, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (29, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (30, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (31, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (32, '180.108.191.6', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (33, '49.75.199.166', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (34, '58.211.119.76', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (35, '114.218.33.81', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (36, '114.218.33.81', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (37, '114.218.33.81', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (38, '49.75.199.166', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (39, '114.219.106.149', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (40, '218.0.0.138', '1', 'OK', '10000', '浙江省', '宁波市', '330200', '121.3173974,29.67340916;121.8125439,30.06811904');
INSERT INTO `aliyun_ip` VALUES (41, '218.0.0.138', '1', 'OK', '10000', '浙江省', '宁波市', '330200', '121.3173974,29.67340916;121.8125439,30.06811904');
INSERT INTO `aliyun_ip` VALUES (42, '121.239.68.124', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (43, '58.209.137.36', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (44, '58.209.137.36', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (45, '58.209.137.36', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (46, '58.209.137.36', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (47, '58.209.137.36', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (48, '58.209.137.36', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (49, '58.209.137.36', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (50, '61.155.214.34', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (51, '49.84.169.163', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (52, '49.84.169.163', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (53, '114.219.106.47', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (54, '114.219.106.47', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
INSERT INTO `aliyun_ip` VALUES (55, '117.81.172.201', '1', 'OK', '10000', '江苏省', '苏州市', '320500', '120.4473102,31.15503121;120.8443308,31.49484731');
COMMIT;

-- ----------------------------
-- Table structure for sys_app
-- ----------------------------
DROP TABLE IF EXISTS `sys_app`;
CREATE TABLE `sys_app` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(100) NOT NULL COMMENT '应用名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `active` varchar(1) DEFAULT NULL COMMENT '是否默认激活（Y-是，N-否）',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态（字典 0正常 1停用 2删除）',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统应用表';

-- ----------------------------
-- Records of sys_app
-- ----------------------------
BEGIN;
INSERT INTO `sys_app` VALUES (1, 'ceshi', 'test', 'N', 0, NULL, 0, '2021-05-25 18:14:04', 1, '2021-07-26 16:49:34', 1);
INSERT INTO `sys_app` VALUES (2, 'ceshi1', 'test1', 'N', 0, NULL, 0, '2021-05-25 18:14:22', 1, '2021-07-26 16:49:23', 1);
INSERT INTO `sys_app` VALUES (3, 'ceshi123', '213', 'N', 0, NULL, 0, '2021-05-25 18:14:29', 1, '2021-06-21 21:30:51', 1);
INSERT INTO `sys_app` VALUES (4, '系统应用', 'system', 'Y', 0, '', 1, '2021-05-25 18:15:02', 1, '2021-08-18 17:35:31', 0);
INSERT INTO `sys_app` VALUES (5, '系统工具', 'systool', 'N', 0, '', 1, '2021-05-25 18:15:26', 1, '2021-08-18 16:30:32', 0);
INSERT INTO `sys_app` VALUES (6, 'ceshi', 'test', 'N', 0, NULL, 0, '2021-05-25 18:17:41', 0, '2021-05-26 11:35:21', 1);
INSERT INTO `sys_app` VALUES (7, 'ceshi', 'test1', 'N', 0, NULL, 0, '2021-05-26 09:29:15', 0, '2021-05-26 11:35:26', 1);
INSERT INTO `sys_app` VALUES (8, 'ceshi', 'test11', 'N', 0, NULL, 0, '2021-05-26 09:36:15', 0, '2021-05-26 11:35:30', 1);
INSERT INTO `sys_app` VALUES (9, 'ceshi13', 'test', 'N', 0, NULL, 0, '2021-05-26 11:33:49', 0, '2021-05-26 11:36:23', 1);
INSERT INTO `sys_app` VALUES (10, 'ceshi1', '213', 'N', 0, NULL, 0, '2021-05-26 11:34:00', 0, '2021-05-26 13:53:27', 1);
INSERT INTO `sys_app` VALUES (11, 'ceshi', 'test', 'N', 0, NULL, 0, '2021-05-26 13:30:00', 0, '2021-05-26 13:53:27', 1);
INSERT INTO `sys_app` VALUES (12, '43', '345343534', 'N', 0, NULL, 0, '2021-05-26 13:31:35', 0, '2021-05-26 13:53:27', 1);
INSERT INTO `sys_app` VALUES (13, 'ceshi', 'test1', 'N', 0, NULL, 0, '2021-05-26 13:35:11', 0, '2021-05-26 13:53:27', 1);
INSERT INTO `sys_app` VALUES (14, 'ceshi', 'test145', 'N', 0, NULL, 0, '2021-05-26 13:35:32', 0, '2021-05-26 13:53:27', 1);
INSERT INTO `sys_app` VALUES (15, '2344', '2342', 'N', 0, NULL, 0, '2021-05-26 13:58:06', 0, '2021-06-24 11:30:24', 1);
INSERT INTO `sys_app` VALUES (16, '11', '11', 'N', 0, NULL, 1, '2021-07-01 10:52:52', 0, '2021-07-01 10:53:16', 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `level` tinyint unsigned NOT NULL COMMENT '层级',
  `parent_code` varchar(20) NOT NULL DEFAULT '0' COMMENT '父级行政代码',
  `area_code` varchar(20) NOT NULL DEFAULT '0' COMMENT '行政代码',
  `zip_code` varchar(6) NOT NULL DEFAULT '0' COMMENT '邮政编码',
  `city_code` varchar(6) NOT NULL DEFAULT '' COMMENT '区号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `short_name` varchar(50) NOT NULL DEFAULT '' COMMENT '简称',
  `merger_name` varchar(50) NOT NULL DEFAULT '' COMMENT '组合名',
  `pinyin` varchar(30) NOT NULL DEFAULT '' COMMENT '拼音',
  `lng` decimal(10,6) NOT NULL COMMENT '经度',
  `lat` decimal(10,6) NOT NULL COMMENT '纬度',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '创建人姓名',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '更新人姓名',
  `update_user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_code` (`area_code`) USING BTREE,
  KEY `idx_parent_code` (`parent_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='中国行政地区表';

-- ----------------------------
-- Records of sys_area
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_code_generate
-- ----------------------------
DROP TABLE IF EXISTS `sys_code_generate`;
CREATE TABLE `sys_code_generate` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `author_name` varchar(255) NOT NULL COMMENT '作者姓名',
  `class_name` varchar(255) NOT NULL COMMENT '类名',
  `table_prefix` varchar(255) NOT NULL COMMENT '是否移除表前缀',
  `generate_type` varchar(255) NOT NULL COMMENT '生成位置类型',
  `table_name` varchar(255) NOT NULL COMMENT '数据库表名',
  `package_name` varchar(255) DEFAULT NULL COMMENT '包名称',
  `bus_name` varchar(255) DEFAULT NULL COMMENT '业务名',
  `table_comment` varchar(255) DEFAULT NULL COMMENT '功能名',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '创建人姓名',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '更新人姓名',
  `update_user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='代码生成基础配置';

-- ----------------------------
-- Records of sys_code_generate
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_code_generate_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_code_generate_config`;
CREATE TABLE `sys_code_generate_config` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code_gen_id` bigint DEFAULT NULL COMMENT '代码生成主表ID',
  `column_name` varchar(255) DEFAULT NULL COMMENT '数据库字段名',
  `java_name` varchar(255) DEFAULT NULL COMMENT 'java类字段名',
  `data_type` varchar(255) DEFAULT NULL COMMENT '物理类型',
  `column_comment` varchar(255) DEFAULT NULL COMMENT '字段描述',
  `java_type` varchar(255) DEFAULT NULL COMMENT 'java类型',
  `effect_type` varchar(255) DEFAULT NULL COMMENT '作用类型（字典）',
  `dict_type_code` varchar(255) DEFAULT NULL COMMENT '字典code',
  `whether_table` varchar(255) DEFAULT NULL COMMENT '列表展示',
  `whether_add_update` varchar(255) DEFAULT NULL COMMENT '增改',
  `whether_retract` varchar(255) DEFAULT NULL COMMENT '列表是否缩进（字典）',
  `whether_required` varchar(255) DEFAULT NULL COMMENT '是否必填（字典）',
  `query_whether` varchar(255) DEFAULT NULL COMMENT '是否是查询条件',
  `query_type` varchar(255) DEFAULT NULL COMMENT '查询方式',
  `column_key` varchar(255) DEFAULT NULL COMMENT '主键',
  `column_key_name` varchar(255) DEFAULT NULL COMMENT '主外键名称',
  `whether_common` varchar(255) DEFAULT NULL COMMENT '是否是通用字段',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '创建人姓名',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '更新人姓名',
  `update_user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='代码生成详细配置';

-- ----------------------------
-- Records of sys_code_generate_config
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `value` varchar(255) NOT NULL COMMENT '值',
  `sys_flag` char(1) NOT NULL COMMENT '是否是系统参数（Y-是，N-否）',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态（字典 0正常 1停用 2删除）',
  `group_code` varchar(255) NOT NULL DEFAULT 'DEFAULT' COMMENT '常量所属分类的编码，来自于“常量的分类”字典',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
BEGIN;
INSERT INTO `sys_config` VALUES (1, '邮箱发件人', 'email_from', 'test@136.com', 'Y', 0, 'EMAIL', '', 1, '2021-07-04 12:49:49', 1, '2021-09-30 15:07:23', 0);
INSERT INTO `sys_config` VALUES (2, '邮箱是否开启ssl', 'email_ssl', 'True', 'Y', 0, 'EMAIL', '', 1, '2021-07-04 12:50:39', 0, '2021-07-04 12:50:39', 0);
INSERT INTO `sys_config` VALUES (3, '邮箱端口', 'email_port', '465', 'Y', 0, 'EMAIL', '', 1, '2021-07-04 12:51:21', 0, '2021-07-04 12:51:21', 0);
INSERT INTO `sys_config` VALUES (4, '邮箱密码', 'email_password', '123456', 'Y', 0, 'EMAIL', '', 1, '2021-07-04 12:52:01', 1, '2021-09-30 15:07:34', 0);
INSERT INTO `sys_config` VALUES (5, '邮箱用户名', 'email_username', 'test@136.com', 'Y', 0, 'EMAIL', '', 1, '2021-07-04 12:52:40', 1, '2021-09-30 15:07:48', 0);
INSERT INTO `sys_config` VALUES (6, '邮箱host', 'email_host', 'smtp.exmail.qq.com', 'Y', 0, 'EMAIL', '', 1, '2021-07-04 12:53:16', 1, '2021-08-18 16:13:16', 0);
INSERT INTO `sys_config` VALUES (7, '系统全称', 'site_name', 'IPSoul后台管理', 'Y', 0, 'DEFAULT', '', 1, '2021-07-29 18:07:10', 0, '2021-07-29 18:07:10', 0);
INSERT INTO `sys_config` VALUES (8, '系统简称', 'nick_name', '后台管理', 'Y', 0, 'DEFAULT', '', 1, '2021-07-29 18:07:39', 0, '2021-07-29 18:07:39', 0);
INSERT INTO `sys_config` VALUES (9, '静态资源存储位置', 'ctx_path', '', 'Y', 0, 'DEFAULT', '', 1, '2021-07-29 18:08:20', 1, '2021-09-30 15:12:05', 0);
INSERT INTO `sys_config` VALUES (10, '系统版本', 'version', 'v1.0.0', 'Y', 0, 'DEFAULT', '', 1, '2021-07-29 18:09:41', 0, '2021-07-29 18:09:41', 0);
INSERT INTO `sys_config` VALUES (11, '阿里云AppCode', 'aliyunAppCode', '阿里云AppCode', 'Y', 0, 'ALIYUN_SMS', '阿里云根据IP获取地理位置接口AppCode', 1, '2021-08-04 19:19:02', 1, '2021-09-30 15:08:46', 0);
INSERT INTO `sys_config` VALUES (12, '阿里云AccessKeyId', 'aliyunAccessKeyId', '阿里云AccessKeyId', 'Y', 0, 'TENCENT_SMS', '', 1, '2021-08-05 18:09:24', 1, '2021-09-30 15:08:53', 0);
INSERT INTO `sys_config` VALUES (13, '阿里云AccessKeySecret', 'aliyunAccessKeySecret', '阿里云AccessKeySecret', 'Y', 0, 'ALIYUN_SMS', '', 1, '2021-08-05 18:10:08', 1, '2021-09-30 15:09:02', 0);
INSERT INTO `sys_config` VALUES (14, '阿里云Endpoint', 'aliyunEndpoint', '阿里云Endpoint', 'Y', 0, 'ALIYUN_SMS', '', 1, '2021-08-05 18:10:42', 1, '2021-09-30 15:09:13', 0);
INSERT INTO `sys_config` VALUES (15, '阿里云Bucket', 'aliyunBucket', 'tpadmin', 'Y', 0, 'ALIYUN_SMS', '', 1, '2021-08-05 18:11:26', 1, '2021-09-30 15:09:24', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_id` bigint NOT NULL COMMENT '字典类型id',
  `value` text NOT NULL COMMENT '值',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `sort` int NOT NULL COMMENT '排序',
  `status` tinyint NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1360606105914732546 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统字典值表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_data` VALUES (1, 1, '正常', '0', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 21:47:04', 0);
INSERT INTO `sys_dict_data` VALUES (2, 1, '停用', '1', 100, 0, '', 0, '2021-05-31 15:40:17', 0, '2021-06-24 21:47:07', 0);
INSERT INTO `sys_dict_data` VALUES (3, 1, '删除', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 21:47:10', 0);
INSERT INTO `sys_dict_data` VALUES (4, 2, '男', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:17:13', 0);
INSERT INTO `sys_dict_data` VALUES (5, 2, '女', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:17:15', 0);
INSERT INTO `sys_dict_data` VALUES (6, 2, '未知', '3', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:17:17', 0);
INSERT INTO `sys_dict_data` VALUES (7, 3, '默认常量', 'DEFAULT', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:18:14', 0);
INSERT INTO `sys_dict_data` VALUES (8, 3, '阿里云短信', 'ALIYUN_SMS', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:18:16', 0);
INSERT INTO `sys_dict_data` VALUES (9, 3, '腾讯云短信', 'TENCENT_SMS', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:18:19', 0);
INSERT INTO `sys_dict_data` VALUES (10, 3, '邮件配置', 'EMAIL', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:18:22', 0);
INSERT INTO `sys_dict_data` VALUES (11, 3, '文件上传路径', 'FILE_PATH', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:18:24', 0);
INSERT INTO `sys_dict_data` VALUES (12, 3, 'Oauth配置', 'OAUTH', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:18:27', 0);
INSERT INTO `sys_dict_data` VALUES (13, 4, '否', 'N', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:19:13', 0);
INSERT INTO `sys_dict_data` VALUES (14, 4, '是', 'Y', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:19:15', 0);
INSERT INTO `sys_dict_data` VALUES (15, 5, '登录', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:27:41', 0);
INSERT INTO `sys_dict_data` VALUES (16, 5, '登出', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:27:44', 0);
INSERT INTO `sys_dict_data` VALUES (17, 6, '目录', '0', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:28:37', 0);
INSERT INTO `sys_dict_data` VALUES (18, 6, '菜单', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:28:39', 0);
INSERT INTO `sys_dict_data` VALUES (19, 6, '按钮', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:28:42', 0);
INSERT INTO `sys_dict_data` VALUES (20, 7, '未发送', '0', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:31:39', 0);
INSERT INTO `sys_dict_data` VALUES (21, 7, '发送成功', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:31:42', 0);
INSERT INTO `sys_dict_data` VALUES (22, 7, '发送失败', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:31:44', 0);
INSERT INTO `sys_dict_data` VALUES (23, 7, '失效', '3', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:31:47', 0);
INSERT INTO `sys_dict_data` VALUES (24, 8, '无', '0', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:33:41', 0);
INSERT INTO `sys_dict_data` VALUES (25, 8, '组件', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:33:43', 0);
INSERT INTO `sys_dict_data` VALUES (26, 8, '内链', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:33:49', 0);
INSERT INTO `sys_dict_data` VALUES (27, 8, '外链', '3', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 22:33:51', 0);
INSERT INTO `sys_dict_data` VALUES (28, 9, '系统权重', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:00:54', 0);
INSERT INTO `sys_dict_data` VALUES (29, 9, '业务权重', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:00:56', 0);
INSERT INTO `sys_dict_data` VALUES (30, 10, '全部数据', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:01:33', 0);
INSERT INTO `sys_dict_data` VALUES (31, 10, '本部门及以下数据', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:01:37', 0);
INSERT INTO `sys_dict_data` VALUES (32, 10, '本部门数据', '3', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:01:39', 0);
INSERT INTO `sys_dict_data` VALUES (33, 10, '仅本人数据', '4', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:01:42', 0);
INSERT INTO `sys_dict_data` VALUES (34, 10, '自定义数据', '5', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:01:44', 0);
INSERT INTO `sys_dict_data` VALUES (35, 11, 'app', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:02:15', 0);
INSERT INTO `sys_dict_data` VALUES (36, 11, 'pc', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:02:17', 0);
INSERT INTO `sys_dict_data` VALUES (37, 11, '其他', '3', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:02:20', 0);
INSERT INTO `sys_dict_data` VALUES (38, 12, '其它', '0', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:29', 0);
INSERT INTO `sys_dict_data` VALUES (39, 12, '增加', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:31', 0);
INSERT INTO `sys_dict_data` VALUES (40, 12, '删除', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:34', 0);
INSERT INTO `sys_dict_data` VALUES (41, 12, '编辑', '3', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:37', 0);
INSERT INTO `sys_dict_data` VALUES (42, 12, '更新', '4', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:39', 0);
INSERT INTO `sys_dict_data` VALUES (43, 12, '查询', '5', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:41', 0);
INSERT INTO `sys_dict_data` VALUES (44, 12, '详情', '6', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:43', 0);
INSERT INTO `sys_dict_data` VALUES (45, 12, '树', '7', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:45', 0);
INSERT INTO `sys_dict_data` VALUES (46, 12, '导入', '8', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:46', 0);
INSERT INTO `sys_dict_data` VALUES (47, 12, '导出', '9', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:48', 0);
INSERT INTO `sys_dict_data` VALUES (48, 12, '授权', '10', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:50', 0);
INSERT INTO `sys_dict_data` VALUES (49, 12, '强退', '11', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:51', 0);
INSERT INTO `sys_dict_data` VALUES (50, 12, '清空', '12', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:53', 0);
INSERT INTO `sys_dict_data` VALUES (51, 12, '修改状态', '13', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:03:55', 0);
INSERT INTO `sys_dict_data` VALUES (52, 13, '阿里云', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:04:35', 0);
INSERT INTO `sys_dict_data` VALUES (53, 13, '腾讯云', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:04:37', 0);
INSERT INTO `sys_dict_data` VALUES (54, 13, 'minio', '3', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:04:38', 0);
INSERT INTO `sys_dict_data` VALUES (55, 13, '本地', '4', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:04:40', 0);
INSERT INTO `sys_dict_data` VALUES (56, 14, '运行', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:05:14', 0);
INSERT INTO `sys_dict_data` VALUES (57, 14, '停止', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:05:18', 0);
INSERT INTO `sys_dict_data` VALUES (58, 15, '通知', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:05:50', 0);
INSERT INTO `sys_dict_data` VALUES (59, 15, '公告', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:05:53', 0);
INSERT INTO `sys_dict_data` VALUES (60, 16, '草稿', '0', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:06:29', 0);
INSERT INTO `sys_dict_data` VALUES (61, 16, '发布', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:06:31', 0);
INSERT INTO `sys_dict_data` VALUES (62, 16, '撤回', '2', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:06:33', 0);
INSERT INTO `sys_dict_data` VALUES (63, 16, '删除', '3', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:06:35', 0);
INSERT INTO `sys_dict_data` VALUES (64, 17, '是', 'true', 100, 2, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:07:16', 0);
INSERT INTO `sys_dict_data` VALUES (65, 17, '否', 'false', 100, 2, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:07:24', 0);
INSERT INTO `sys_dict_data` VALUES (66, 18, '未读', '0', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:07:42', 0);
INSERT INTO `sys_dict_data` VALUES (67, 18, '已读', '1', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:07:45', 0);
INSERT INTO `sys_dict_data` VALUES (68, 19, '下载压缩包', '1', 1, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:08:14', 0);
INSERT INTO `sys_dict_data` VALUES (69, 19, '生成到本地项目', '2', 2, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:08:17', 0);
INSERT INTO `sys_dict_data` VALUES (70, 20, '输入框', 'input', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:08:44', 0);
INSERT INTO `sys_dict_data` VALUES (71, 20, '时间选择', 'datepicker', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:09:05', 0);
INSERT INTO `sys_dict_data` VALUES (72, 20, '下拉框', 'select', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:09:08', 0);
INSERT INTO `sys_dict_data` VALUES (73, 20, '单选框', 'radio', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:09:11', 0);
INSERT INTO `sys_dict_data` VALUES (74, 20, '开关', 'switch', 100, 2, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:09:13', 0);
INSERT INTO `sys_dict_data` VALUES (75, 20, '多选框', 'checkbox', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:09:17', 0);
INSERT INTO `sys_dict_data` VALUES (76, 20, '数字输入框', 'inputnumber', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:09:21', 0);
INSERT INTO `sys_dict_data` VALUES (77, 20, '文本域', 'textarea', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:09:25', 0);
INSERT INTO `sys_dict_data` VALUES (78, 21, '等于', 'eq', 1, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:09:49', 0);
INSERT INTO `sys_dict_data` VALUES (79, 21, '模糊', 'like', 2, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:10:11', 0);
INSERT INTO `sys_dict_data` VALUES (80, 21, '大于', 'gt', 3, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:10:13', 0);
INSERT INTO `sys_dict_data` VALUES (81, 21, '小于', 'lt', 4, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:10:18', 0);
INSERT INTO `sys_dict_data` VALUES (82, 21, '不等于', 'ne', 7, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:10:21', 0);
INSERT INTO `sys_dict_data` VALUES (83, 21, '大于等于', 'ge', 5, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:10:24', 0);
INSERT INTO `sys_dict_data` VALUES (84, 21, '小于等于', 'le', 6, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:10:27', 0);
INSERT INTO `sys_dict_data` VALUES (85, 21, '不为空', 'isNotNull', 8, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:10:30', 0);
INSERT INTO `sys_dict_data` VALUES (86, 22, 'Long', 'Long', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:10:51', 0);
INSERT INTO `sys_dict_data` VALUES (87, 22, 'String', 'String', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:11:06', 0);
INSERT INTO `sys_dict_data` VALUES (88, 22, 'Date', 'Date', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:11:08', 0);
INSERT INTO `sys_dict_data` VALUES (89, 22, 'Integer', 'Integer', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:11:11', 0);
INSERT INTO `sys_dict_data` VALUES (90, 22, 'boolean', 'boolean', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:11:13', 0);
INSERT INTO `sys_dict_data` VALUES (91, 22, 'int', 'int', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:11:16', 0);
INSERT INTO `sys_dict_data` VALUES (92, 22, 'double', 'double', 100, 0, NULL, 0, '2021-05-31 15:40:17', 0, '2021-06-24 23:11:18', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `sort` int NOT NULL COMMENT '排序',
  `status` tinyint NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1358470065111252995 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_type` VALUES (1, '通用状态', 'common_status', 100, 0, '', 0, '2021-05-31 15:40:45', 0, '2021-06-24 21:49:36', 0);
INSERT INTO `sys_dict_type` VALUES (2, '性别', 'sex', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 22:17:27', 0);
INSERT INTO `sys_dict_type` VALUES (3, '常量的分类', 'consts_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 22:17:52', 0);
INSERT INTO `sys_dict_type` VALUES (4, '是否', 'yes_or_no', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 22:18:36', 0);
INSERT INTO `sys_dict_type` VALUES (5, '访问类型', 'vis_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 22:27:37', 0);
INSERT INTO `sys_dict_type` VALUES (6, '菜单类型', 'menu_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 22:28:33', 0);
INSERT INTO `sys_dict_type` VALUES (7, '发送类型', 'send_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 22:31:08', 0);
INSERT INTO `sys_dict_type` VALUES (8, '打开方式', 'open_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 22:33:19', 0);
INSERT INTO `sys_dict_type` VALUES (9, '菜单权重', 'menu_weight', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:00:45', 0);
INSERT INTO `sys_dict_type` VALUES (10, '数据范围类型', 'data_scope_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:01:28', 0);
INSERT INTO `sys_dict_type` VALUES (11, '短信发送来源', 'sms_send_source', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:02:10', 0);
INSERT INTO `sys_dict_type` VALUES (12, '操作类型', 'op_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:03:25', 0);
INSERT INTO `sys_dict_type` VALUES (13, '文件存储位置', 'file_storage_location', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:04:32', 0);
INSERT INTO `sys_dict_type` VALUES (14, '运行状态', 'run_status', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:05:21', 0);
INSERT INTO `sys_dict_type` VALUES (15, '通知公告类型', 'notice_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:05:45', 0);
INSERT INTO `sys_dict_type` VALUES (16, '通知公告状态', 'notice_status', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:06:00', 0);
INSERT INTO `sys_dict_type` VALUES (17, '是否boolean', 'yes_true_false', 100, 2, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:07:20', 0);
INSERT INTO `sys_dict_type` VALUES (18, '阅读状态', 'read_status', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:07:48', 0);
INSERT INTO `sys_dict_type` VALUES (19, '代码生成方式', 'code_gen_create_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:08:19', 0);
INSERT INTO `sys_dict_type` VALUES (20, '代码生成作用类型', 'code_gen_effect_type', 100, 0, '', 0, '2021-05-31 15:40:45', 1, '2021-08-18 16:17:17', 0);
INSERT INTO `sys_dict_type` VALUES (21, '代码生成查询类型', 'code_gen_query_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:10:32', 0);
INSERT INTO `sys_dict_type` VALUES (22, '代码生成java类型', 'code_gen_java_type', 100, 0, NULL, 0, '2021-05-31 15:40:45', 0, '2021-06-24 23:10:36', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_emp
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp`;
CREATE TABLE `sys_emp` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `job_num` varchar(100) DEFAULT NULL COMMENT '工号',
  `org_id` bigint NOT NULL COMMENT '所属机构id',
  `org_name` varchar(100) NOT NULL COMMENT '所属机构名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='员工表';

-- ----------------------------
-- Records of sys_emp
-- ----------------------------
BEGIN;
INSERT INTO `sys_emp` VALUES (1, '001', 11, '普腾一部');
INSERT INTO `sys_emp` VALUES (2, 'qw', 2, '华夏集团北京分公司');
INSERT INTO `sys_emp` VALUES (3, '100', 11, '普腾一部');
INSERT INTO `sys_emp` VALUES (4, '', 1265476890672672769, '华夏集团北京分公司');
INSERT INTO `sys_emp` VALUES (5, '001', 4, '研发部');
INSERT INTO `sys_emp` VALUES (7, '002', 5, '企划部');
INSERT INTO `sys_emp` VALUES (8, '111', 7, '财务部');
COMMIT;

-- ----------------------------
-- Table structure for sys_emp_ext_org_pos
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp_ext_org_pos`;
CREATE TABLE `sys_emp_ext_org_pos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `emp_id` bigint NOT NULL COMMENT '员工id',
  `org_id` bigint NOT NULL COMMENT '机构id',
  `pos_id` bigint NOT NULL COMMENT '岗位id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1332141421776715792 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='员工附属机构岗位表';

-- ----------------------------
-- Records of sys_emp_ext_org_pos
-- ----------------------------
BEGIN;
INSERT INTO `sys_emp_ext_org_pos` VALUES (1332141421759938561, 1280700700074041345, 1265476890672672772, 1265476890672672789);
INSERT INTO `sys_emp_ext_org_pos` VALUES (1332141421776715778, 1280700700074041345, 1265476890672672773, 1265476890672672790);
INSERT INTO `sys_emp_ext_org_pos` VALUES (1332141421776715779, 5, 1, 4);
INSERT INTO `sys_emp_ext_org_pos` VALUES (1332141421776715780, 7, 10, 1);
INSERT INTO `sys_emp_ext_org_pos` VALUES (1332141421776715781, 7, 11, 1);
INSERT INTO `sys_emp_ext_org_pos` VALUES (1332141421776715787, 8, 11, 1);
INSERT INTO `sys_emp_ext_org_pos` VALUES (1332141421776715788, 8, 11, 2);
INSERT INTO `sys_emp_ext_org_pos` VALUES (1332141421776715790, 3, 10, 1);
INSERT INTO `sys_emp_ext_org_pos` VALUES (1332141421776715791, 1, 11, 2);
COMMIT;

-- ----------------------------
-- Table structure for sys_emp_pos
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp_pos`;
CREATE TABLE `sys_emp_pos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `emp_id` bigint NOT NULL COMMENT '员工id',
  `pos_id` bigint NOT NULL COMMENT '职位id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1332214497600180240 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='员工职位关联表';

-- ----------------------------
-- Records of sys_emp_pos
-- ----------------------------
BEGIN;
INSERT INTO `sys_emp_pos` VALUES (1281042262003867649, 1280709549107552257, 1265476890672672787);
INSERT INTO `sys_emp_pos` VALUES (1332141421789298689, 1280700700074041345, 1265476890672672790);
INSERT INTO `sys_emp_pos` VALUES (1332141567281315841, 1275735541155614721, 1265476890672672787);
INSERT INTO `sys_emp_pos` VALUES (1332214497600180226, 1332142087677001730, 1265476890672672788);
INSERT INTO `sys_emp_pos` VALUES (1332214497600180227, 5, 4);
INSERT INTO `sys_emp_pos` VALUES (1332214497600180228, 5, 3);
INSERT INTO `sys_emp_pos` VALUES (1332214497600180229, 7, 3);
INSERT INTO `sys_emp_pos` VALUES (1332214497600180230, 7, 4);
INSERT INTO `sys_emp_pos` VALUES (1332214497600180235, 8, 1);
INSERT INTO `sys_emp_pos` VALUES (1332214497600180237, 3, 2);
INSERT INTO `sys_emp_pos` VALUES (1332214497600180238, 1, 2);
INSERT INTO `sys_emp_pos` VALUES (1332214497600180239, 2, 2);
COMMIT;

-- ----------------------------
-- Table structure for sys_file_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_file_info`;
CREATE TABLE `sys_file_info` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `file_location` tinyint NOT NULL COMMENT '文件存储位置（1:阿里云，2:腾讯云，3:minio，4:本地）',
  `file_bucket` varchar(1000) DEFAULT NULL COMMENT '文件仓库',
  `file_origin_name` varchar(100) NOT NULL COMMENT '文件名称（上传时候的文件名）',
  `file_suffix` varchar(50) DEFAULT NULL COMMENT '文件后缀',
  `file_size_kb` bigint DEFAULT NULL COMMENT '文件大小kb',
  `file_size_info` varchar(100) DEFAULT NULL COMMENT '文件大小信息，计算后的',
  `file_object_name` varchar(100) NOT NULL COMMENT '存储到bucket的名称（文件唯一标识id）',
  `file_path` varchar(1000) DEFAULT NULL COMMENT '存储路径',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='文件信息表';

-- ----------------------------
-- Records of sys_file_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` bigint NOT NULL COMMENT '父id',
  `pids` text COMMENT '父ids',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `type` tinyint NOT NULL DEFAULT '1' COMMENT '菜单类型（字典 0目录 1菜单 2按钮）',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `router` varchar(255) DEFAULT 'javascript:;' COMMENT '路由地址',
  `permission` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `application` varchar(50) NOT NULL COMMENT '应用分类（应用编码）',
  `weight` tinyint DEFAULT NULL COMMENT '权重（字典 1系统权重 2业务权重）',
  `sort` int NOT NULL COMMENT '排序',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态（字典 0正常 1停用 2删除）',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES (1, 0, NULL, '组织架构', 'sys_mgr', 0, 'layui-icon-user', '', '', 'system', 1, 1, 0, '', 0, '2021-05-26 16:09:54', 1, '2021-07-01 11:24:37', 0);
INSERT INTO `sys_menu` VALUES (2, 1, NULL, '用户管理', 'sys_user_mgr', 1, '', '/admin/user/index', '', 'system', 1, 1, 0, '', 0, '2021-05-26 16:12:30', 1, '2021-07-28 17:58:19', 0);
INSERT INTO `sys_menu` VALUES (3, 1, NULL, '机构管理', 'sys_org_mgr', 1, '', '/admin/org/index', '', 'system', 1, 2, 0, '', 0, '2021-05-26 16:13:39', 1, '2021-07-28 17:58:26', 0);
INSERT INTO `sys_menu` VALUES (4, 1, NULL, '职位管理', 'sys_pos_mgr', 1, '', '/admin/pos/index', '', 'system', 1, 3, 0, '', 0, '2021-05-26 16:14:48', 1, '2021-07-28 17:58:36', 0);
INSERT INTO `sys_menu` VALUES (5, 0, NULL, '权限管理', 'auth_manager', 0, 'layui-icon-auz', '', '', 'system', 1, 2, 0, '', 0, '2021-05-26 16:17:30', 0, '2021-05-26 16:17:30', 0);
INSERT INTO `sys_menu` VALUES (6, 5, NULL, '应用管理', 'sys_app_mgr', 1, '', '/admin/app/index', '', 'system', 1, 1, 0, '', 0, '2021-05-26 16:24:39', 1, '2021-07-28 18:04:42', 0);
INSERT INTO `sys_menu` VALUES (7, 5, NULL, '菜单管理', 'sys_menu_mgr', 1, '', '/admin/menu/index', '', 'system', 1, 2, 0, '', 0, '2021-05-26 16:25:09', 1, '2021-07-28 18:04:48', 0);
INSERT INTO `sys_menu` VALUES (8, 5, NULL, '角色管理', 'sys_role_mgr', 1, '', '/admin/role/index', '', 'system', 1, 3, 0, '', 0, '2021-05-26 16:25:47', 1, '2021-07-28 18:04:53', 0);
INSERT INTO `sys_menu` VALUES (9, 0, NULL, '开发管理', 'system_tools', 0, 'layui-icon-fonts-code', '', '', 'system', 1, 3, 0, '', 0, '2021-05-26 16:48:16', 1, '2021-08-18 17:26:12', 0);
INSERT INTO `sys_menu` VALUES (10, 9, NULL, '系统配置', 'system_tools_config', 1, '', '/admin/config/index', '', 'system', 1, 1, 0, '', 0, '2021-05-26 16:49:19', 1, '2021-07-28 18:02:45', 0);
INSERT INTO `sys_menu` VALUES (11, 9, NULL, '邮件发送', 'sys_email_mgr', 1, '', '/admin/email/index', '', 'system', 1, 2, 0, '', 0, '2021-05-26 17:51:25', 1, '2021-07-28 18:04:16', 0);
INSERT INTO `sys_menu` VALUES (12, 9, NULL, '短信管理', 'sys_sms_mgr', 1, '', '/admin/sms/index', '', 'system', 1, 3, 0, '', 0, '2021-05-26 17:52:17', 1, '2021-07-28 18:04:22', 0);
INSERT INTO `sys_menu` VALUES (13, 9, NULL, '字典管理', 'sys_dict_mgr', 1, '', '/admin/dict_type/index', '', 'system', 1, 4, 0, '', 0, '2021-05-26 17:53:02', 1, '2021-07-28 18:04:27', 0);
INSERT INTO `sys_menu` VALUES (14, 9, NULL, '接口文档', 'sys_swagger_mgr', 1, '', '/apidoc/index.html', '', 'system', 1, 5, 1, '', 0, '2021-05-26 17:54:33', 1, '2021-07-28 18:04:33', 0);
INSERT INTO `sys_menu` VALUES (15, 0, NULL, '日志管理', 'sys_log_mgr', 0, 'layui-icon-log', '', '', 'system', 1, 5, 0, '', 0, '2021-05-26 17:58:07', 1, '2021-08-18 17:30:13', 0);
INSERT INTO `sys_menu` VALUES (16, 15, NULL, '访问日志', 'sys_log_mgr_vis_log', 1, '', '/admin/vis_log/index', '', 'system', 1, 1, 0, '', 0, '2021-05-26 17:59:55', 1, '2021-07-28 18:04:05', 0);
INSERT INTO `sys_menu` VALUES (17, 15, NULL, '操作日志', 'sys_log_mgr_op_log', 1, '', '/admin/op_log/index', '', 'system', 1, 2, 0, '', 0, '2021-05-26 18:01:36', 1, '2021-07-28 18:04:10', 0);
INSERT INTO `sys_menu` VALUES (18, 0, NULL, '文件管理', 'sys_file_mgr', 0, 'layui-icon-file-b', '', '', 'system', 1, 6, 1, '', 0, '2021-05-27 09:42:04', 0, '2021-07-01 11:18:35', 0);
INSERT INTO `sys_menu` VALUES (19, 18, NULL, '系统文件', 'sys_file_mgr_sys_file', 1, '', '/admin/file_info/index', '', 'system', 1, 1, 10, '', 0, '2021-05-27 09:43:24', 1, '2021-07-28 18:03:53', 0);
INSERT INTO `sys_menu` VALUES (20, 18, NULL, '在线文档', 'sys_file_mgr_sys_online_file', 1, '', '/admin/file_info/onlineIndex', '', 'system', 1, 2, 1, '', 0, '2021-05-27 09:44:12', 1, '2021-07-28 18:03:59', 0);
INSERT INTO `sys_menu` VALUES (21, 0, NULL, '定时任务', 'sys_timers', 0, 'layui-icon-console', '', '', 'system', 1, 7, 1, '', 0, '2021-05-27 09:45:27', 0, '2021-07-01 10:17:03', 0);
INSERT INTO `sys_menu` VALUES (22, 21, NULL, '任务管理', 'sys_timers_mgr', 1, '', '/admin/timers/index', '', 'system', 1, 1, 1, '', 0, '2021-05-27 09:46:08', 1, '2021-07-28 18:03:45', 0);
INSERT INTO `sys_menu` VALUES (23, 0, NULL, '通知公告', 'sys_notice', 0, 'layui-icon-speaker', '', '', 'system', 1, 8, 0, '', 0, '2021-05-27 09:47:38', 0, '2021-05-27 09:47:38', 0);
INSERT INTO `sys_menu` VALUES (24, 23, NULL, '公告管理', 'sys_notice_mgr', 1, '', '/admin/notice/index', '', 'system', 1, 1, 0, '', 0, '2021-05-27 09:48:26', 1, '2021-07-28 18:03:40', 0);
INSERT INTO `sys_menu` VALUES (25, 23, NULL, '已收公告', 'sys_notice_mgr_received', 1, '', '/admin/notice/received_page', '', 'system', 1, 2, 0, '', 0, '2021-05-27 09:49:10', 1, '2021-07-28 18:03:35', 0);
INSERT INTO `sys_menu` VALUES (26, 0, NULL, '系统监控', 'sys_monitor_mgr', 0, 'layui-icon-chart-screen', '', '', 'system', 1, 9, 1, '', 0, '2021-05-27 09:50:48', 0, '2021-07-01 10:16:44', 0);
INSERT INTO `sys_menu` VALUES (27, 26, NULL, '服务监控', 'sys_monitor_mgr_machine_monitor', 1, '', '/admin/machine/index', '', 'system', 1, 1, 1, '', 0, '2021-05-27 09:52:23', 1, '2021-07-28 18:03:05', 0);
INSERT INTO `sys_menu` VALUES (28, 26, NULL, '在线用户', 'sys_monitor_mgr_online_user', 1, '', '/admin/online_user/index', '', 'system', 1, 2, 1, '', 0, '2021-05-27 09:53:05', 1, '2021-07-28 18:03:12', 0);
INSERT INTO `sys_menu` VALUES (29, 26, NULL, '数据监控', 'sys_monitor_mgr_druid', 1, '', '/druid/login.html', '', 'system', 1, 3, 0, '', 0, '2021-05-27 09:53:39', 1, '2021-07-28 18:03:19', 0);
INSERT INTO `sys_menu` VALUES (30, 0, NULL, '主控面板', 'system_index', 0, 'layui-icon-home', '', '', 'system', 1, 0, 0, '', 1, '2021-06-24 10:36:20', 0, '2021-06-24 10:38:34', 0);
INSERT INTO `sys_menu` VALUES (31, 30, NULL, '工作台', 'system_index_workplace', 1, '', '/admin/index/home', '', 'system', 1, 1, 0, '', 1, '2021-06-24 10:40:59', 1, '2021-07-28 18:02:57', 0);
INSERT INTO `sys_menu` VALUES (32, 30, NULL, '控制台', 'system_index_console', 1, '', '/admin/index/console', '', 'system', 1, 2, 0, '', 1, '2021-06-24 10:42:09', 1, '2021-07-28 18:02:51', 0);
INSERT INTO `sys_menu` VALUES (33, 2, NULL, '用户新增', 'sys_user_add_mgr', 2, '', '/admin/user/form;/admin/user/save', 'sysUser:add', 'system', 1, 1, 0, '', 1, '2021-07-19 16:19:13', 1, '2021-07-28 17:58:53', 0);
INSERT INTO `sys_menu` VALUES (34, 2, NULL, '用户编辑', 'sys_user_edit_mgr', 2, '', '/admin/user/form;/admin/user/save', 'sysUser:edit', 'system', 1, 2, 0, '', 1, '2021-07-19 17:27:35', 1, '2021-07-28 17:59:03', 0);
INSERT INTO `sys_menu` VALUES (35, 2, NULL, '用户删除', 'sys_user_del_mgr', 2, '', '/admin/user/delete', 'sysUser:delete', 'system', 1, 3, 0, '', 1, '2021-07-19 17:29:10', 1, '2021-07-28 17:59:29', 0);
INSERT INTO `sys_menu` VALUES (36, 2, NULL, '用户授权角色', 'sys_user_role_mgr', 2, '', '/admin/user/grant_role;/admin/user/post_grant_role;/admin/user/own_role;/admin/role/drop_down', 'sysUser:grantRole', 'system', 1, 4, 0, '', 1, '2021-07-19 17:47:51', 1, '2021-07-28 17:59:43', 0);
INSERT INTO `sys_menu` VALUES (37, 2, NULL, '用户授权数据', 'sys_user_data_mgr', 2, '', '/admin/user/grant_data;/admin/user/post_grant_data;/admin/user/own_data;/admin/org/tree', 'sysUser:grantData', 'system', 1, 5, 0, '', 1, '2021-07-28 14:03:38', 1, '2021-07-28 17:59:54', 0);
INSERT INTO `sys_menu` VALUES (38, 3, NULL, '机构新增', 'sys_org', 2, '', '/admin/org/form;/admin/org/save', 'sysOrg:add', 'system', 1, 1, 0, '', 1, '2021-07-28 17:45:06', 1, '2021-07-28 18:00:04', 0);
INSERT INTO `sys_menu` VALUES (39, 3, NULL, '机构编辑', 'sss', 2, '', '/admin/org/form;/admin/org/save', 'sysOrg:edit', 'system', 1, 2, 0, '', 1, '2021-07-28 17:46:26', 1, '2021-07-28 18:00:11', 0);
INSERT INTO `sys_menu` VALUES (40, 3, NULL, '机构删除', 'ssss', 2, '', '/admin/org/delete', 'sysOrg:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (41, 4, NULL, '职位新增', 'sys_org', 2, '', '/admin/pos/form;/admin/pos/save', 'sysPos:add', 'system', 1, 1, 0, '', 1, '2021-07-28 17:45:06', 1, '2021-07-28 18:10:42', 0);
INSERT INTO `sys_menu` VALUES (42, 4, NULL, '职位编辑', 'sss', 2, '', '/admin/pos/form;/admin/pos/save', 'sysPos:edit', 'system', 1, 2, 0, '', 1, '2021-07-28 17:46:26', 1, '2021-07-28 18:10:38', 0);
INSERT INTO `sys_menu` VALUES (43, 4, NULL, '职位删除', 'ssss', 2, '', '/admin/pos/delete', 'sysPos:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:10:35', 0);
INSERT INTO `sys_menu` VALUES (44, 6, NULL, '应用新增', '', 2, '', '/admin/app/form;/admin/app/save', 'sysApp:add', 'system', 1, 1, 0, '', 1, '2021-07-28 17:45:06', 1, '2021-07-28 18:00:04', 0);
INSERT INTO `sys_menu` VALUES (45, 6, NULL, '应用编辑', '', 2, '', '/admin/app/form;/admin/app/save', 'sysApp:edit', 'system', 1, 2, 0, '', 1, '2021-07-28 17:46:26', 1, '2021-07-28 18:00:11', 0);
INSERT INTO `sys_menu` VALUES (46, 6, NULL, '应用删除', '', 2, '', '/admin/app/delete', 'sysApp:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (47, 6, NULL, '应用设为默认', '', 2, '', '/admin/app/set_as_default', 'sysApp:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (48, 7, NULL, '菜单新增', '', 2, '', '/admin/menu/form;/admin/menu/save', 'sysMenu:add', 'system', 1, 1, 0, '', 1, '2021-07-28 17:45:06', 1, '2021-07-28 18:00:04', 0);
INSERT INTO `sys_menu` VALUES (49, 7, NULL, '菜单编辑', '', 2, '', '/admin/menu/form;/admin/menu/save', 'sysMenu:edit', 'system', 1, 2, 0, '', 1, '2021-07-28 17:46:26', 1, '2021-07-28 18:00:11', 0);
INSERT INTO `sys_menu` VALUES (50, 7, NULL, '菜单删除', '', 2, '', '/admin/menu/delete', 'sysMenu:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (51, 8, NULL, '角色新增', '', 2, '', '/admin/role/form;/admin/role/save', 'sysRole:add', 'system', 1, 1, 0, '', 1, '2021-07-28 17:45:06', 1, '2021-07-28 18:00:04', 0);
INSERT INTO `sys_menu` VALUES (52, 8, NULL, '角色编辑', '', 2, '', '/admin/role/form;/admin/role/save', 'sysRole:edit', 'system', 1, 2, 0, '', 1, '2021-07-28 17:46:26', 1, '2021-07-28 18:00:11', 0);
INSERT INTO `sys_menu` VALUES (53, 8, NULL, '角色删除', '', 2, '', '/admin/role/delete', 'sysRole:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (54, 8, NULL, '角色授权菜单', '', 2, '', '/admin/role/delete', 'sysRole:grantMenu', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (55, 8, NULL, '角色授权数据', '', 2, '', '/admin/role/delete', 'sysRole:grantData', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (56, 10, NULL, '配置新增', '', 2, '', '/admin/config/form;/admin/config/save', 'sysConfig:add', 'system', 1, 1, 0, '', 1, '2021-07-28 17:45:06', 1, '2021-07-28 18:00:04', 0);
INSERT INTO `sys_menu` VALUES (57, 10, NULL, '配置编辑', '', 2, '', '/admin/config/form;/admin/config/save', 'sysConfig:edit', 'system', 1, 2, 0, '', 1, '2021-07-28 17:46:26', 1, '2021-07-28 18:00:11', 0);
INSERT INTO `sys_menu` VALUES (58, 10, NULL, '配置删除', '', 2, '', '/admin/config/delete', 'sysConfig:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (59, 13, NULL, '字典类型新增', '', 2, '', '/admin/dict_type/form;/admin/dict_type/save', 'sysDictType:add', 'system', 1, 1, 0, '', 1, '2021-07-28 17:45:06', 1, '2021-07-28 18:00:04', 0);
INSERT INTO `sys_menu` VALUES (60, 13, NULL, '字典类型编辑', '', 2, '', '/admin/dict_type/form;/admin/dict_type/save', 'sysDictType:edit', 'system', 1, 2, 0, '', 1, '2021-07-28 17:46:26', 1, '2021-07-28 18:00:11', 0);
INSERT INTO `sys_menu` VALUES (61, 13, NULL, '字典类型删除', '', 2, '', '/admin/dict_type/delete', 'sysDictType:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (62, 8, NULL, '字典值', '', 2, '', '/admin/dict_data/index;/admin/dict_data/page', 'sysDictData:list', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (63, 13, NULL, '字典值新增', '', 2, '', '/admin/dict_data/form;/admin/dict_data/save', 'sysDictData:add', 'system', 1, 1, 0, '', 1, '2021-07-28 17:45:06', 1, '2021-07-28 18:00:04', 0);
INSERT INTO `sys_menu` VALUES (64, 13, NULL, '字典值型编辑', '', 2, '', '/admin/dict_data/form;/admin/dict_data/save', 'sysDictData:edit', 'system', 1, 2, 0, '', 1, '2021-07-28 17:46:26', 1, '2021-07-28 18:00:11', 0);
INSERT INTO `sys_menu` VALUES (65, 13, NULL, '字典值删除', '', 2, '', '/admin/dict_data/delete', 'sysDictData:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (66, 16, NULL, '访问日志清空', '', 2, '', '/admin/vis_log/delete', 'sysVisLoge:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
INSERT INTO `sys_menu` VALUES (67, 17, NULL, '操作日志清空', '', 2, '', '/admin/op_log/delete', 'sysOpLog:delete', 'system', 1, 3, 0, '', 1, '2021-07-28 17:47:28', 1, '2021-07-28 18:00:22', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_message
-- ----------------------------
DROP TABLE IF EXISTS `sys_message`;
CREATE TABLE `sys_message` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '消息id',
  `title` varchar(50) DEFAULT NULL COMMENT '消息标题',
  `content` text COMMENT '消息内容',
  `type` int DEFAULT NULL COMMENT '消息类别，字典（1通知 2私信 3待办）',
  `send_type` int DEFAULT NULL COMMENT '发送类别，字典（1直接发送 2定时发送）',
  `business_data` varchar(1000) DEFAULT NULL COMMENT '业务数据，JSON格式',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '创建人姓名',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '更新人姓名',
  `update_user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1352534482631999491 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='消息表';

-- ----------------------------
-- Records of sys_message
-- ----------------------------
BEGIN;
INSERT INTO `sys_message` VALUES (1352534482631999490, '重磅！XiaoNuo-Layui单体版本发布！', '<p>新版本已正式发布，欢迎围观</p>\r\n<p><a style=\"color:red\" href=\"https://my.oschina.net/u/3359742/blog/4779878\" target=\"_blank\">https://my.oschina.net/u/3359742/blog/4779878</a></p>\r\n<ul>\r\n<li>纯手研发搭建框架脚手架，在自己用的时候，也为各位小伙伴打下坚固的接私活利器。</li>\r\n<li>后续我们会行发多个版本，将适配多个数据库环境，国产化环境，并且根据多年经验会出相关系统中用到的案例，提供给大家使用！</li>\r\n<li>如需了解我们更多，请移步官网：<a href=\"https://xiaonuo.vip/\" target=\"_blank\" rel=\"nofollow\">https://xiaonuo.vip</a></li>\r\n<li>当然，有问题讨论的小伙伴还可以加入我们的QQ技术群：732230670，一起学习讨论。</li>\r\n</ul>', 1, 1, '{\"id\":\"1336605943982546946\"}', '2021-01-22 17:37:42', NULL, 0, '', '2021-05-31 15:42:23', 0, '', '2021-05-31 15:42:23', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_message_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_message_user`;
CREATE TABLE `sys_message_user` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `message_id` bigint NOT NULL COMMENT '消息id',
  `sender_id` bigint NOT NULL COMMENT '发送人id，系统发送则为-1',
  `receiver_id` bigint NOT NULL COMMENT '接收人id',
  `status` tinyint NOT NULL COMMENT '状态（字典 0未读 1已读）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1352534482631999491 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='消息人员关联表';

-- ----------------------------
-- Records of sys_message_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_message_user` VALUES (1336611879405051905, 1352534482631999490, -1, 1275735541155614721, 1);
INSERT INTO `sys_message_user` VALUES (1352534482631999490, 1352534482631999490, -1, 1280709549107552257, 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(1000) DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `type` tinyint NOT NULL COMMENT '类型（字典 1通知 2公告）',
  `public_user_id` bigint NOT NULL COMMENT '发布人id',
  `public_user_name` varchar(100) NOT NULL COMMENT '发布人姓名',
  `public_org_id` bigint DEFAULT NULL COMMENT '发布机构id',
  `public_org_name` varchar(50) DEFAULT NULL COMMENT '发布机构名称',
  `public_time` datetime DEFAULT NULL COMMENT '发布时间',
  `cancel_time` datetime DEFAULT NULL COMMENT '撤回时间',
  `status` tinyint NOT NULL COMMENT '状态（字典 0草稿 1发布 2撤回 3删除）',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='通知表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
BEGIN;
INSERT INTO `sys_notice` VALUES (1, '11111111', '<p>123123</p>\n<p>213</p>\n<p>213</p>\n<p>123</p>\n<p>123</p>\n<p>21</p>\n<p>3</p>\n<p>123</p>\n<p>12333332323232</p>', 1, 1, '超级管理员', 1265476890672672769, '华夏集团北京分公司', '2021-06-28 22:37:20', '2021-06-27 03:57:23', 1, 1, '2021-06-27 01:09:54', 1, '2021-06-28 22:37:20', 0);
INSERT INTO `sys_notice` VALUES (2, '213', '<p>qwe</p>\n<p>qwe</p>', 2, 1, '超级管理员', 11, '普腾一部', '2021-07-16 17:34:48', NULL, 1, 1, '2021-06-28 22:35:31', 1, '2021-07-16 22:43:29', 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_notice_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice_user`;
CREATE TABLE `sys_notice_user` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `notice_id` bigint NOT NULL COMMENT '通知公告id',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态（字典 0未读 1已读）',
  `read_time` datetime DEFAULT NULL COMMENT '阅读时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统用户数据范围表';

-- ----------------------------
-- Records of sys_notice_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_notice_user` VALUES (4, 1, 1, 1, '2021-09-07 06:44:47');
INSERT INTO `sys_notice_user` VALUES (5, 1, 2, 1, '2021-06-16 02:13:41');
INSERT INTO `sys_notice_user` VALUES (6, 1, 3, 0, NULL);
INSERT INTO `sys_notice_user` VALUES (7, 2, 2, 0, NULL);
INSERT INTO `sys_notice_user` VALUES (8, 2, 3, 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_oauth_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth_user`;
CREATE TABLE `sys_oauth_user` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uuid` varchar(255) NOT NULL COMMENT '第三方平台的用户唯一id',
  `access_token` varchar(255) DEFAULT NULL COMMENT '用户授权的token',
  `nick_name` varchar(255) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(500) DEFAULT NULL COMMENT '头像',
  `blog` varchar(255) DEFAULT NULL COMMENT '用户网址',
  `company` varchar(255) DEFAULT NULL COMMENT '所在公司',
  `location` varchar(255) DEFAULT NULL COMMENT '位置',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `gender` varchar(50) DEFAULT NULL COMMENT '性别',
  `source` varchar(255) DEFAULT NULL COMMENT '用户来源',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '创建人姓名',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '更新人姓名',
  `update_user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='第三方认证用户信息表';

-- ----------------------------
-- Records of sys_oauth_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_op_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_op_log`;
CREATE TABLE `sys_op_log` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `op_type` tinyint DEFAULT '1' COMMENT '操作类型',
  `success` char(1) DEFAULT NULL COMMENT '是否执行成功（Y-是，N-否）',
  `message` text COMMENT '具体消息',
  `ip` varchar(255) DEFAULT NULL COMMENT 'ip',
  `location` varchar(255) DEFAULT NULL COMMENT '地址',
  `browser` varchar(255) DEFAULT NULL COMMENT '浏览器',
  `os` varchar(255) DEFAULT NULL COMMENT '操作系统',
  `url` varchar(500) DEFAULT NULL COMMENT '请求地址',
  `class_name` varchar(500) DEFAULT NULL COMMENT '类名称',
  `method_name` varchar(500) DEFAULT NULL COMMENT '方法名称',
  `req_method` varchar(255) DEFAULT NULL COMMENT '请求方式（GET POST PUT DELETE)',
  `param` text COMMENT '请求参数',
  `result` longtext COMMENT '返回结果',
  `op_time` datetime DEFAULT NULL COMMENT '操作时间',
  `account` varchar(50) DEFAULT NULL COMMENT '操作账号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统操作日志表';

-- ----------------------------
-- Records of sys_op_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_op_log` VALUES (1, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"10\",\"name\":\"系统版本\",\"code\":\"version\",\"sys_flag\":\"Y\",\"group_code\":\"DEFAULT\",\"value\":\"v1.0.0\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 16:10:15', 'admin');
INSERT INTO `sys_op_log` VALUES (2, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"9\",\"name\":\"静态资源存储位置\",\"code\":\"ctx_path\",\"sys_flag\":\"Y\",\"group_code\":\"DEFAULT\",\"value\":\"https:\\/\\/ipsoul.oss-cn-shanghai.aliyuncs.com\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 16:13:08', 'admin');
INSERT INTO `sys_op_log` VALUES (3, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"11\",\"name\":\"阿里云AppCode\",\"code\":\"aliyunAppCode\",\"sys_flag\":\"Y\",\"group_code\":\"ALIYUN_SMS\",\"value\":\"26b534f7342849808938d4761c13d42f\",\"remark\":\"阿里云根据IP获取地理位置接口AppCode\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 16:13:12', 'admin');
INSERT INTO `sys_op_log` VALUES (4, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"6\",\"name\":\"邮箱host\",\"code\":\"email_host\",\"sys_flag\":\"Y\",\"group_code\":\"EMAIL\",\"value\":\"smtp.exmail.qq.com\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 16:13:16', 'admin');
INSERT INTO `sys_op_log` VALUES (5, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/dict_type/save', 'DictType', 'save', 'POST', '{\"id\":\"20\",\"name\":\"代码生成作用类型\",\"code\":\"code_gen_effect_type\",\"sort\":\"100\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 16:17:17', 'admin');
INSERT INTO `sys_op_log` VALUES (6, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"5\",\"active\":\"N\",\"name\":\"系统工具\",\"code\":\"systool\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 16:30:32', 'admin');
INSERT INTO `sys_op_log` VALUES (7, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"5\",\"active\":\"N\",\"name\":\"系统工具\",\"code\":\"systool\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 16:37:09', 'admin');
INSERT INTO `sys_op_log` VALUES (8, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"5\",\"active\":\"N\",\"name\":\"系统工具\",\"code\":\"systool\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 16:39:22', 'admin');
INSERT INTO `sys_op_log` VALUES (9, '菜单管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/menu/save', 'Menu', 'save', 'POST', '{\"id\":\"9\",\"pid\":\"0\",\"name\":\"开发管理\",\"type\":\"0\",\"icon\":\"layui-icon-fonts-code\",\"router\":\"\",\"permission\":\"\",\"weight\":\"1\",\"application\":\"system\",\"sort\":\"3\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 17:26:12', 'admin');
INSERT INTO `sys_op_log` VALUES (10, '菜单管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/menu/save', 'Menu', 'save', 'POST', '{\"id\":\"15\",\"pid\":\"0\",\"name\":\"日志管理\",\"type\":\"0\",\"icon\":\"layui-icon-log\",\"router\":\"\",\"permission\":\"\",\"weight\":\"1\",\"application\":\"system\",\"sort\":\"5\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 17:30:13', 'admin');
INSERT INTO `sys_op_log` VALUES (11, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"5\",\"active\":\"N\",\"name\":\"系统工具\",\"code\":\"systool\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 17:30:21', 'admin');
INSERT INTO `sys_op_log` VALUES (12, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"5\",\"active\":\"N\",\"name\":\"系统工具\",\"code\":\"systool\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 17:33:00', 'admin');
INSERT INTO `sys_op_log` VALUES (13, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"4\",\"active\":\"Y\",\"name\":\"系统应用\",\"code\":\"system\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 17:35:31', 'admin');
INSERT INTO `sys_op_log` VALUES (14, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"5\",\"active\":\"N\",\"name\":\"系统工具\",\"code\":\"systool\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 17:37:21', 'admin');
INSERT INTO `sys_op_log` VALUES (15, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"4\",\"active\":\"Y\",\"name\":\"系统应用\",\"code\":\"system\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 17:38:01', 'admin');
INSERT INTO `sys_op_log` VALUES (16, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"4\",\"active\":\"Y\",\"name\":\"系统应用\",\"code\":\"system\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 17:51:17', 'admin');
INSERT INTO `sys_op_log` VALUES (17, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"4\",\"active\":\"Y\",\"name\":\"系统应用\",\"code\":\"system\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 17:55:59', 'admin');
INSERT INTO `sys_op_log` VALUES (18, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"4\",\"active\":\"Y\",\"name\":\"系统应用\",\"code\":\"system\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 18:00:23', 'admin');
INSERT INTO `sys_op_log` VALUES (19, '应用管理_编辑', 1, 'Y', '成功编辑了1条数据', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', '/admin/app/save', 'App', 'save', 'POST', '{\"id\":\"4\",\"active\":\"Y\",\"name\":\"系统应用\",\"code\":\"system\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-08-18 18:02:07', 'admin');
INSERT INTO `sys_op_log` VALUES (20, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"1\",\"name\":\"邮箱发件人\",\"code\":\"email_from\",\"sys_flag\":\"Y\",\"group_code\":\"EMAIL\",\"value\":\"jiaerxiao@putonsoft.com\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-09-30 15:07:07', 'admin');
INSERT INTO `sys_op_log` VALUES (21, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"1\",\"name\":\"邮箱发件人\",\"code\":\"email_from\",\"sys_flag\":\"Y\",\"group_code\":\"EMAIL\",\"value\":\"test@136.com\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-09-30 15:07:23', 'admin');
INSERT INTO `sys_op_log` VALUES (22, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"4\",\"name\":\"邮箱密码\",\"code\":\"email_password\",\"sys_flag\":\"Y\",\"group_code\":\"EMAIL\",\"value\":\"123456\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-09-30 15:07:34', 'admin');
INSERT INTO `sys_op_log` VALUES (23, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"5\",\"name\":\"邮箱用户名\",\"code\":\"email_username\",\"sys_flag\":\"Y\",\"group_code\":\"EMAIL\",\"value\":\"test@136.com\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-09-30 15:07:48', 'admin');
INSERT INTO `sys_op_log` VALUES (24, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"9\",\"name\":\"静态资源存储位置\",\"code\":\"ctx_path\",\"sys_flag\":\"Y\",\"group_code\":\"DEFAULT\",\"value\":\"\\/\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-09-30 15:08:31', 'admin');
INSERT INTO `sys_op_log` VALUES (25, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"11\",\"name\":\"阿里云AppCode\",\"code\":\"aliyunAppCode\",\"sys_flag\":\"Y\",\"group_code\":\"ALIYUN_SMS\",\"value\":\"阿里云AppCode\",\"remark\":\"阿里云根据IP获取地理位置接口AppCode\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-09-30 15:08:46', 'admin');
INSERT INTO `sys_op_log` VALUES (26, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"12\",\"name\":\"阿里云AccessKeyId\",\"code\":\"aliyunAccessKeyId\",\"sys_flag\":\"Y\",\"group_code\":\"TENCENT_SMS\",\"value\":\"阿里云AccessKeyId\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-09-30 15:08:53', 'admin');
INSERT INTO `sys_op_log` VALUES (27, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"13\",\"name\":\"阿里云AccessKeySecret\",\"code\":\"aliyunAccessKeySecret\",\"sys_flag\":\"Y\",\"group_code\":\"ALIYUN_SMS\",\"value\":\"阿里云AccessKeySecret\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-09-30 15:09:02', 'admin');
INSERT INTO `sys_op_log` VALUES (28, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"14\",\"name\":\"阿里云Endpoint\",\"code\":\"aliyunEndpoint\",\"sys_flag\":\"Y\",\"group_code\":\"ALIYUN_SMS\",\"value\":\"阿里云Endpoint\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-09-30 15:09:13', 'admin');
INSERT INTO `sys_op_log` VALUES (29, '系统配置_编辑', 1, 'Y', '成功编辑了1条数据', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', '/admin/config/save', 'Config', 'save', 'POST', '{\"id\":\"15\",\"name\":\"阿里云Bucket\",\"code\":\"aliyunBucket\",\"sys_flag\":\"Y\",\"group_code\":\"ALIYUN_SMS\",\"value\":\"tpadmin\",\"remark\":\"\"}', '{\"success\":true,\"code\":200,\"message\":\"操作成功\",\"data\":[]}', '2021-09-30 15:09:24', 'admin');
COMMIT;

-- ----------------------------
-- Table structure for sys_org
-- ----------------------------
DROP TABLE IF EXISTS `sys_org`;
CREATE TABLE `sys_org` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` bigint NOT NULL COMMENT '父id',
  `pids` text COMMENT '父ids',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `sort` int NOT NULL COMMENT '排序',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态（字典 0正常 1停用 2删除）',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统组织机构表';

-- ----------------------------
-- Records of sys_org
-- ----------------------------
BEGIN;
INSERT INTO `sys_org` VALUES (1, 0, '[0]', '华夏集团', 'hxjt', 100, 0, '', 0, '2021-05-27 11:38:25', 1, '2021-07-14 19:59:28', 0);
INSERT INTO `sys_org` VALUES (2, 1, '[0],[1]', '华夏集团北京分公司', 'hxjt_bj', 100, 0, '', 0, '2021-05-27 11:38:25', 1, '2021-07-14 20:00:47', 0);
INSERT INTO `sys_org` VALUES (3, 1, '[0],[1]', '华夏集团成都分公司', 'hxjt_cd', 100, 0, '', 0, '2021-05-27 11:38:25', 1, '2021-07-14 20:01:44', 0);
INSERT INTO `sys_org` VALUES (4, 2, '[0],[1],[2]', '研发部', 'hxjt_bj_yfb', 100, 0, '', 0, '2021-05-27 11:38:25', 1, '2021-07-14 20:00:52', 0);
INSERT INTO `sys_org` VALUES (5, 2, '[0],[1],[2]', '企划部', 'hxjt_bj_qhb', 100, 0, '', 0, '2021-05-27 11:38:25', 1, '2021-07-14 20:01:07', 0);
INSERT INTO `sys_org` VALUES (6, 3, '[0],[1],[3]', '市场部', 'hxjt_cd_scb', 100, 0, '', 0, '2021-05-27 11:38:25', 1, '2021-07-14 20:02:12', 0);
INSERT INTO `sys_org` VALUES (7, 3, '[0],[1],[3]', '财务部', 'hxjt_cd_cwb', 100, 0, '', 0, '2021-05-27 11:38:25', 1, '2021-07-14 20:02:20', 0);
INSERT INTO `sys_org` VALUES (8, 6, '[0],[1],[3],[6]', '市场部二部', 'hxjt_cd_scb_2b', 100, 0, '123', 0, '2021-05-27 11:38:25', 1, '2021-07-14 20:02:23', 0);
INSERT INTO `sys_org` VALUES (9, 6, '[0],[1],[3],[6]', '市场部一部', 'hxjt_cd_scb_1b', 1, 0, '', 0, '2021-05-27 13:44:19', 1, '2021-07-14 20:02:27', 0);
INSERT INTO `sys_org` VALUES (10, 0, '[0]', '普腾集团', 'ptjt', 1, 0, '', 1, '2021-07-02 23:25:04', 1, '2021-07-14 19:59:21', 0);
INSERT INTO `sys_org` VALUES (11, 10, '[0],[10]', '普腾一部', 'ptjt_1b', 1, 0, '', 1, '2021-07-02 23:25:49', 1, '2021-07-14 19:59:24', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_pos
-- ----------------------------
DROP TABLE IF EXISTS `sys_pos`;
CREATE TABLE `sys_pos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `sort` int NOT NULL COMMENT '排序',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态（字典 0正常 1停用 2删除）',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `CODE_UNI` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统职位表';

-- ----------------------------
-- Records of sys_pos
-- ----------------------------
BEGIN;
INSERT INTO `sys_pos` VALUES (1, '总经理', 'zjl', 100, 0, NULL, 0, '2021-05-31 15:44:37', 0, '2021-07-01 20:12:32', 0);
INSERT INTO `sys_pos` VALUES (2, '副总经理', 'fzjl', 100, 0, NULL, 0, '2021-05-31 15:44:37', 0, '2021-07-01 20:12:34', 0);
INSERT INTO `sys_pos` VALUES (3, '部门经理', 'bmjl', 100, 0, '', 0, '2021-05-31 15:44:37', 1, '2021-07-01 20:13:18', 0);
INSERT INTO `sys_pos` VALUES (4, '工作人员', 'gzry', 100, 0, '123', 0, '2021-05-31 15:44:37', 1, '2021-07-01 20:13:23', 0);
INSERT INTO `sys_pos` VALUES (5, 'erw', 'wer', 2, 0, 'r wer', 1, '2021-07-01 20:14:01', 0, '2021-07-01 20:19:10', 1);
INSERT INTO `sys_pos` VALUES (6, 'wer', 'werwerwer', 3, 0, 'wer', 1, '2021-07-01 20:18:25', 0, '2021-07-01 20:19:10', 1);
INSERT INTO `sys_pos` VALUES (7, 'tr', 'rt', 2, 0, 'wer', 1, '2021-07-17 12:10:49', 1, '2021-07-17 12:10:57', 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `sort` int NOT NULL COMMENT '序号',
  `data_scope_type` tinyint NOT NULL DEFAULT '1' COMMENT '数据范围类型（字典 1全部数据 2本部门及以下数据 3本部门数据 4仅本人数据 5自定义数据）',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态（字典 0正常 1停用 2删除）',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, '组织架构管理员', 'ent_manager_role', 1, 1, 0, '', 1, '2021-07-11 11:23:12', 0, '2021-07-11 15:19:48', 0);
INSERT INTO `sys_role` VALUES (2, '权限管理员', 'auth_role', 2, 1, 0, '', 1, '2021-07-11 11:23:34', 0, '2021-07-11 15:19:52', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_data_scope`;
CREATE TABLE `sys_role_data_scope` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` bigint NOT NULL COMMENT '角色id',
  `org_id` bigint NOT NULL COMMENT '机构id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1292060127645429763 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统角色数据范围表';

-- ----------------------------
-- Records of sys_role_data_scope
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` bigint NOT NULL COMMENT '角色id',
  `menu_id` bigint NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统角色菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` VALUES (53, 2, 30);
INSERT INTO `sys_role_menu` VALUES (54, 2, 31);
INSERT INTO `sys_role_menu` VALUES (55, 2, 32);
INSERT INTO `sys_role_menu` VALUES (56, 2, 1);
INSERT INTO `sys_role_menu` VALUES (57, 2, 2);
INSERT INTO `sys_role_menu` VALUES (58, 2, 33);
INSERT INTO `sys_role_menu` VALUES (59, 2, 34);
INSERT INTO `sys_role_menu` VALUES (60, 2, 35);
INSERT INTO `sys_role_menu` VALUES (61, 2, 36);
INSERT INTO `sys_role_menu` VALUES (62, 2, 37);
INSERT INTO `sys_role_menu` VALUES (63, 2, 3);
INSERT INTO `sys_role_menu` VALUES (64, 2, 38);
INSERT INTO `sys_role_menu` VALUES (65, 2, 39);
INSERT INTO `sys_role_menu` VALUES (66, 2, 40);
INSERT INTO `sys_role_menu` VALUES (67, 2, 4);
INSERT INTO `sys_role_menu` VALUES (68, 2, 41);
INSERT INTO `sys_role_menu` VALUES (69, 2, 42);
INSERT INTO `sys_role_menu` VALUES (70, 2, 43);
INSERT INTO `sys_role_menu` VALUES (71, 2, 5);
INSERT INTO `sys_role_menu` VALUES (72, 2, 6);
INSERT INTO `sys_role_menu` VALUES (73, 2, 44);
INSERT INTO `sys_role_menu` VALUES (74, 2, 45);
INSERT INTO `sys_role_menu` VALUES (75, 2, 47);
INSERT INTO `sys_role_menu` VALUES (76, 2, 46);
INSERT INTO `sys_role_menu` VALUES (77, 2, 7);
INSERT INTO `sys_role_menu` VALUES (78, 2, 48);
INSERT INTO `sys_role_menu` VALUES (79, 2, 49);
INSERT INTO `sys_role_menu` VALUES (80, 2, 50);
INSERT INTO `sys_role_menu` VALUES (81, 2, 8);
INSERT INTO `sys_role_menu` VALUES (82, 2, 51);
INSERT INTO `sys_role_menu` VALUES (83, 2, 52);
INSERT INTO `sys_role_menu` VALUES (84, 2, 62);
INSERT INTO `sys_role_menu` VALUES (85, 2, 55);
INSERT INTO `sys_role_menu` VALUES (86, 2, 54);
INSERT INTO `sys_role_menu` VALUES (87, 2, 53);
INSERT INTO `sys_role_menu` VALUES (88, 2, 9);
INSERT INTO `sys_role_menu` VALUES (89, 2, 10);
INSERT INTO `sys_role_menu` VALUES (90, 2, 56);
INSERT INTO `sys_role_menu` VALUES (91, 2, 57);
INSERT INTO `sys_role_menu` VALUES (92, 2, 58);
INSERT INTO `sys_role_menu` VALUES (93, 2, 11);
INSERT INTO `sys_role_menu` VALUES (94, 2, 12);
INSERT INTO `sys_role_menu` VALUES (95, 2, 13);
INSERT INTO `sys_role_menu` VALUES (96, 2, 59);
INSERT INTO `sys_role_menu` VALUES (97, 2, 63);
INSERT INTO `sys_role_menu` VALUES (98, 2, 60);
INSERT INTO `sys_role_menu` VALUES (99, 2, 64);
INSERT INTO `sys_role_menu` VALUES (100, 2, 61);
INSERT INTO `sys_role_menu` VALUES (101, 2, 65);
INSERT INTO `sys_role_menu` VALUES (102, 2, 15);
INSERT INTO `sys_role_menu` VALUES (103, 2, 16);
INSERT INTO `sys_role_menu` VALUES (104, 2, 66);
INSERT INTO `sys_role_menu` VALUES (105, 2, 17);
INSERT INTO `sys_role_menu` VALUES (106, 2, 67);
INSERT INTO `sys_role_menu` VALUES (107, 2, 23);
INSERT INTO `sys_role_menu` VALUES (108, 2, 24);
INSERT INTO `sys_role_menu` VALUES (109, 2, 25);
COMMIT;

-- ----------------------------
-- Table structure for sys_sms
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms`;
CREATE TABLE `sys_sms` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `phone_numbers` varchar(200) NOT NULL COMMENT '手机号',
  `validate_code` varchar(255) DEFAULT NULL COMMENT '短信验证码',
  `template_code` varchar(255) DEFAULT NULL COMMENT '短信模板ID',
  `biz_id` varchar(255) DEFAULT NULL COMMENT '回执id，可根据该id查询具体的发送状态',
  `status` tinyint NOT NULL COMMENT '发送状态（字典 0 未发送，1 发送成功，2 发送失败，3 失效）',
  `source` tinyint NOT NULL COMMENT '来源（字典 1 app， 2 pc， 3 其他）',
  `invalid_time` datetime DEFAULT NULL COMMENT '失效时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='短信信息发送表';

-- ----------------------------
-- Records of sys_sms
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_timers
-- ----------------------------
DROP TABLE IF EXISTS `sys_timers`;
CREATE TABLE `sys_timers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '定时器id',
  `timer_name` varchar(255) DEFAULT '' COMMENT '任务名称',
  `action_class` varchar(255) DEFAULT NULL COMMENT '执行任务的class的类名（实现了TimerTaskRunner接口的类的全称）',
  `cron` varchar(255) DEFAULT '' COMMENT '定时任务表达式',
  `job_status` tinyint DEFAULT '0' COMMENT '状态（字典 1运行  2停止）',
  `remark` text COMMENT '备注',
  `create_user_id` int NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '创建人姓名',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` int NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '更新人姓名',
  `update_user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1304971718170832899 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='定时任务';

-- ----------------------------
-- Records of sys_timers
-- ----------------------------
BEGIN;
INSERT INTO `sys_timers` VALUES (1288760324837851137, '定时同步缓存常量', 'com.cn.xiaonuo.sys.modular.timer.tasks.RefreshConstantsTaskRunner', '0 0/1 * * * ?', 1, NULL, 0, '', '2021-05-31 15:46:15', 0, '', '2021-05-31 15:46:15', 0);
INSERT INTO `sys_timers` VALUES (1304971718170832898, '定时打印一句话', 'com.cn.xiaonuo.sys.modular.timer.tasks.SystemOutTaskRunner', '0 0 * * * ? *', 1, NULL, 0, '', '2021-05-31 15:46:15', 0, '', '2021-05-31 15:46:15', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `account` varchar(50) NOT NULL COMMENT '账号',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `avatar` bigint DEFAULT NULL COMMENT '头像',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `sex` tinyint NOT NULL COMMENT '性别(字典 1男 2女 3未知)',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) DEFAULT NULL COMMENT '手机',
  `last_login_ip` varchar(100) DEFAULT NULL COMMENT '最后登陆IP',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `admin_type` tinyint NOT NULL DEFAULT '0' COMMENT '管理员类型（0超级管理员 1非管理员）',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态（字典 0正常 1冻结 2删除）',
  `remark` text COMMENT '备注',
  `create_user_id` bigint NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `create_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '创建人姓名',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_user_id` bigint NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `update_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '更新人姓名',
  `update_user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (1, 'admin', '0a989ebc4a77b56a6e2bb7b19d995d185ce44090c13e2984b7ecc6d446d4b61ea9991b76a4c2f04b1b4d244841449454', '超级管理员', '超级管理员', NULL, '2020-03-18', 1, 'superAdmin@qq.com', '15228937093', '110.184.43.91', '2021-01-22 17:50:10', 1, 0, NULL, 1, '', '2021-05-31 15:48:13', 1, '', '2021-08-18 16:04:31', 0);
INSERT INTO `sys_user` VALUES (2, 'yubaoshan', '0a989ebc4a77b56a6e2bb7b19d995d185ce44090c13e2984b7ecc6d446d4b61ea9991b76a4c2f04b1b4d244841449454', 'Await', '俞宝山', NULL, '1992-10-03', 1, 'await183@qq.com', '18200001102', '110.184.43.91', '2021-01-22 17:49:51', 2, 0, NULL, 1, '', '2021-05-31 15:48:13', 1, '', '2021-08-18 16:04:34', 0);
INSERT INTO `sys_user` VALUES (3, 'xuyuxiang', '0a989ebc4a77b56a6e2bb7b19d995d185ce44090c13e2984b7ecc6d446d4b61ea9991b76a4c2f04b1b4d244841449454', '就是那个锅', '徐玉祥', NULL, '2020-07-01', 1, 'jiaerxiao@putonsoft.com', '18200001100', '110.184.43.91', '2021-01-22 17:48:40', 2, 0, NULL, 1, '', '2021-05-31 15:48:13', 1, '超级管理员', '2021-08-18 16:04:36', 0);
INSERT INTO `sys_user` VALUES (5, 'jiaerxiao', '0a989ebc4a77b56a6e2bb7b19d995d185ce44090c13e2984b7ecc6d446d4b61ea9991b76a4c2f04b1b4d244841449454', NULL, '贾二小', NULL, '2021-07-11', 1, 'jiaerxiao@putonsoft.com', '18734735949', NULL, NULL, 2, 0, NULL, 1, '', '2021-07-11 19:44:11', 1, '', '2021-08-18 16:04:39', 0);
INSERT INTO `sys_user` VALUES (6, 'jiaerxiao1', '14e1b600b1fd579f47433b88e8d85291', NULL, '贾二小', NULL, '2021-07-11', 1, 'jiaerxiao@putonsoft.com', '18734735949', NULL, NULL, 2, 0, NULL, 1, '', '2021-07-11 19:44:11', 1, '', '2021-07-28 10:19:15', 0);
INSERT INTO `sys_user` VALUES (7, 'jiaerxiao11', '14e1b600b1fd579f47433b88e8d85291', NULL, '12', NULL, '2021-07-05', 1, 'jiaerxiao@putonsoft.com', '18734735949', NULL, NULL, 2, 0, NULL, 1, '', '2021-07-11 19:47:06', 1, '', '2021-07-28 10:19:17', 0);
INSERT INTO `sys_user` VALUES (8, 'jiaerxiao1111', '14e1b600b1fd579f47433b88e8d85291', NULL, '组织架构管理员', NULL, '2021-07-14', 2, 'jiaerxiao@putonsoft.com', '18734735949', NULL, NULL, 2, 0, NULL, 1, '', '2021-07-14 18:54:50', 1, '', '2021-07-28 10:19:20', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_data_scope`;
CREATE TABLE `sys_user_data_scope` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `org_id` bigint NOT NULL COMMENT '机构id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统用户数据范围表';

-- ----------------------------
-- Records of sys_user_data_scope
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_data_scope` VALUES (7, 1, 3);
INSERT INTO `sys_user_data_scope` VALUES (8, 1, 6);
INSERT INTO `sys_user_data_scope` VALUES (9, 1, 9);
INSERT INTO `sys_user_data_scope` VALUES (10, 1, 8);
INSERT INTO `sys_user_data_scope` VALUES (11, 1, 7);
INSERT INTO `sys_user_data_scope` VALUES (12, 2, 2);
INSERT INTO `sys_user_data_scope` VALUES (13, 2, 4);
INSERT INTO `sys_user_data_scope` VALUES (14, 2, 5);
INSERT INTO `sys_user_data_scope` VALUES (15, 3, 10);
INSERT INTO `sys_user_data_scope` VALUES (16, 3, 11);
INSERT INTO `sys_user_data_scope` VALUES (17, 3, 4);
INSERT INTO `sys_user_data_scope` VALUES (18, 3, 5);
INSERT INTO `sys_user_data_scope` VALUES (19, 3, 3);
INSERT INTO `sys_user_data_scope` VALUES (20, 3, 6);
INSERT INTO `sys_user_data_scope` VALUES (21, 3, 9);
INSERT INTO `sys_user_data_scope` VALUES (22, 3, 8);
INSERT INTO `sys_user_data_scope` VALUES (23, 3, 7);
INSERT INTO `sys_user_data_scope` VALUES (24, 8, 2);
INSERT INTO `sys_user_data_scope` VALUES (25, 8, 4);
INSERT INTO `sys_user_data_scope` VALUES (26, 8, 5);
INSERT INTO `sys_user_data_scope` VALUES (27, 7, 3);
INSERT INTO `sys_user_data_scope` VALUES (28, 7, 6);
INSERT INTO `sys_user_data_scope` VALUES (29, 7, 9);
INSERT INTO `sys_user_data_scope` VALUES (30, 7, 8);
INSERT INTO `sys_user_data_scope` VALUES (31, 7, 7);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `role_id` bigint NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统用户角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES (1, 3, 1);
INSERT INTO `sys_user_role` VALUES (2, 2, 2);
INSERT INTO `sys_user_role` VALUES (3, 1, 1);
INSERT INTO `sys_user_role` VALUES (4, 1, 2);
INSERT INTO `sys_user_role` VALUES (5, 8, 1);
INSERT INTO `sys_user_role` VALUES (6, 7, 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_vis_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_vis_log`;
CREATE TABLE `sys_vis_log` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `success` char(1) DEFAULT NULL COMMENT '是否执行成功（Y-是，N-否）',
  `message` text COMMENT '具体消息',
  `ip` varchar(255) DEFAULT NULL COMMENT 'ip',
  `location` varchar(255) DEFAULT NULL COMMENT '地址',
  `browser` varchar(255) DEFAULT NULL COMMENT '浏览器',
  `os` varchar(255) DEFAULT NULL COMMENT '操作系统',
  `vis_type` tinyint NOT NULL COMMENT '操作类型（字典 1登入 2登出）',
  `vis_time` datetime DEFAULT NULL COMMENT '访问时间',
  `account` varchar(50) DEFAULT NULL COMMENT '访问账号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统访问日志表';

-- ----------------------------
-- Records of sys_vis_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_vis_log` VALUES (1, '用户登出', 'Y', '登出', '127.0.0.1', '', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 2, '2021-08-18 15:56:21', NULL);
INSERT INTO `sys_vis_log` VALUES (2, '用户登录', 'Y', '登录', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-18 16:05:30', 'admin');
INSERT INTO `sys_vis_log` VALUES (3, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-18 16:10:05', 'admin');
INSERT INTO `sys_vis_log` VALUES (4, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-18 17:05:07', 'admin');
INSERT INTO `sys_vis_log` VALUES (5, '用户登录', 'Y', '登录', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-18 17:26:04', 'admin');
INSERT INTO `sys_vis_log` VALUES (6, '用户登录', 'Y', '登录', '180.108.191.6', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-18 17:37:50', 'admin');
INSERT INTO `sys_vis_log` VALUES (7, '用户登录', 'Y', '登录', '49.75.199.166', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-18 18:15:10', 'admin');
INSERT INTO `sys_vis_log` VALUES (8, '用户登录', 'Y', '登录', '58.211.119.76', '江苏省 苏州市', '微信浏览器_7.0.20.1781(0x6700143B)', 'Windows_7', 1, '2021-08-19 08:31:42', 'admin');
INSERT INTO `sys_vis_log` VALUES (9, '用户登录', 'Y', '登录', '114.218.33.81', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-19 10:13:58', 'admin');
INSERT INTO `sys_vis_log` VALUES (10, '用户登出', 'Y', '登出', '114.218.33.81', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 2, '2021-08-19 10:16:25', 'admin');
INSERT INTO `sys_vis_log` VALUES (11, '用户登录', 'Y', '登录', '114.218.33.81', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-19 10:16:34', 'admin');
INSERT INTO `sys_vis_log` VALUES (12, '用户登录', 'Y', '登录', '49.75.199.166', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-19 14:02:48', 'admin');
INSERT INTO `sys_vis_log` VALUES (13, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-19 17:26:37', 'admin');
INSERT INTO `sys_vis_log` VALUES (14, '用户登录', 'Y', '登录', '114.219.106.149', '江苏省 苏州市', 'Chrome_92.0.4515.131', 'Mac OS X_10.15.7', 1, '2021-08-20 09:18:18', 'admin');
INSERT INTO `sys_vis_log` VALUES (15, '用户登录', 'Y', '登录', '218.0.0.138', '浙江省 宁波市', 'Chrome_92.0.4515.159', 'Mac OS X_10.15.7', 1, '2021-08-25 12:03:22', 'admin');
INSERT INTO `sys_vis_log` VALUES (16, '用户登录', 'Y', '登录', '218.0.0.138', '浙江省 宁波市', 'Chrome_92.0.4515.159', 'Mac OS X_10.15.7', 1, '2021-08-26 13:18:51', 'admin');
INSERT INTO `sys_vis_log` VALUES (17, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_92.0.4515.159', 'Mac OS X_10.15.7', 1, '2021-09-02 09:35:15', 'admin');
INSERT INTO `sys_vis_log` VALUES (18, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-02 16:17:34', 'admin');
INSERT INTO `sys_vis_log` VALUES (19, '用户登录', 'Y', '登录', '121.239.68.124', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-02 16:20:01', 'admin');
INSERT INTO `sys_vis_log` VALUES (20, '用户登录', 'Y', '登录', '58.209.137.36', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-03 09:10:24', 'admin');
INSERT INTO `sys_vis_log` VALUES (21, '用户登出', 'Y', '登出', '58.209.137.36', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 2, '2021-09-03 09:11:06', 'admin');
INSERT INTO `sys_vis_log` VALUES (22, '用户登录', 'Y', '登录', '58.209.137.36', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-03 09:11:07', 'admin');
INSERT INTO `sys_vis_log` VALUES (23, '用户登出', 'Y', '登出', '58.209.137.36', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 2, '2021-09-03 09:15:01', 'admin');
INSERT INTO `sys_vis_log` VALUES (24, '用户登录', 'Y', '登录', '58.209.137.36', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-03 09:15:02', 'admin');
INSERT INTO `sys_vis_log` VALUES (25, '用户登录', 'Y', '登录', '58.209.137.36', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-03 09:54:38', 'admin');
INSERT INTO `sys_vis_log` VALUES (26, '用户登录', 'Y', '登录', '58.209.137.36', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-03 11:15:22', 'admin');
INSERT INTO `sys_vis_log` VALUES (27, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-06 11:15:06', 'admin');
INSERT INTO `sys_vis_log` VALUES (28, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-07 06:24:05', 'admin');
INSERT INTO `sys_vis_log` VALUES (29, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-07 08:11:40', 'admin');
INSERT INTO `sys_vis_log` VALUES (30, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-07 08:11:51', 'admin');
INSERT INTO `sys_vis_log` VALUES (31, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-07 08:12:45', 'admin');
INSERT INTO `sys_vis_log` VALUES (32, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-07 08:12:17', 'admin');
INSERT INTO `sys_vis_log` VALUES (33, '用户登录', 'Y', '登录', '61.155.214.34', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-08 10:56:41', 'admin');
INSERT INTO `sys_vis_log` VALUES (34, '用户登录', 'Y', '登录', '49.84.169.163', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-10 10:56:32', 'admin');
INSERT INTO `sys_vis_log` VALUES (35, '用户登录', 'Y', '登录', '49.84.169.163', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-10 11:29:19', 'admin');
INSERT INTO `sys_vis_log` VALUES (36, '用户登录', 'Y', '登录', '114.219.106.47', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-18 11:12:45', 'admin');
INSERT INTO `sys_vis_log` VALUES (37, '用户登录', 'Y', '登录', '114.219.106.47', '江苏省 苏州市', 'Chrome_93.0.4577.63', 'Mac OS X_10.15.7', 1, '2021-09-18 17:16:41', 'admin');
INSERT INTO `sys_vis_log` VALUES (38, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', 1, '2021-09-29 16:33:37', 'admin');
INSERT INTO `sys_vis_log` VALUES (39, '用户登录', 'Y', '登录', '117.81.172.201', '江苏省 苏州市', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', 1, '2021-09-30 13:10:31', 'admin');
INSERT INTO `sys_vis_log` VALUES (40, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', 1, '2021-09-30 15:00:54', 'admin');
INSERT INTO `sys_vis_log` VALUES (41, '用户登录', 'Y', '登录', '127.0.0.1', '', 'Chrome_94.0.4606.61', 'Mac OS X_10.15.7', 1, '2021-09-30 15:12:36', 'admin');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
